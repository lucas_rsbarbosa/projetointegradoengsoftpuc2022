from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User
from django.utils.html import format_html

from .models import Administrator, ContentCreator, Video
# Register your models here.
from .models import Noticia
from django.contrib import messages
from django.utils.translation import ngettext


@admin.action(description='Selecione item(s) para ser(em) aprovado(s)')
def make_published(self, request, queryset):
    updated = queryset.update(status="A")
    queryset.update(aprovado_por=request.user)

    self.message_user(request, ngettext(
        '%d item marcado como aprovado.',
        '%d itens marcados como aprovad0s.',
        updated,
    ) % updated, messages.SUCCESS)


class NoticiaAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("titulo",)}
    exclude = ("criado_por", "aprovado_por", "status")
    list_display = (
        "titulo",
        "slug",
        "is_destaque_home",
        "tipo_noticia",
        "status",
    )
    actions = [make_published]

    def get_actions(self, request):
        actions = super().get_actions(request)

        is_admin = False
        try:
            is_admin = (request.user.administrator is not None)
        except User.administrator.RelatedObjectDoesNotExist:
            pass

        if not is_admin:
            if 'make_published' in actions:
                del actions['make_published']
        return actions

    def save_model(self, request, obj, form, change):
        if not change:
            obj.criado_por = request.user
            obj.save()
        super().save_model(request, obj, form, change)


class VideoAdmin(admin.ModelAdmin):
    exclude = ("criado_por", "aprovado_por", "status")
    list_display = (
        "titulo",
        "is_destaque_home",
        "status",
    )
    actions = [make_published]

    def get_actions(self, request):
        actions = super().get_actions(request)

        is_admin = False
        try:
            is_admin = (request.user.administrator is not None)
        except User.administrator.RelatedObjectDoesNotExist:
            pass

        if not is_admin:
            if 'make_published' in actions:
                del actions['make_published']
        return actions

    def save_model(self, request, obj, form, change):
        if not change:
            obj.criado_por = request.user
            obj.save()
        super().save_model(request, obj, form, change)


# Define an inline admin descriptor for Employee model
# which acts a bit like a singleton
class AdministratorInline(admin.StackedInline):
    model = Administrator
    can_delete = False
    verbose_name_plural = 'Administrators'


class ContentCreatorInline(admin.StackedInline):
    model = ContentCreator
    can_delete = False
    verbose_name_plural = 'Content Creators'


# Define a new User admin
class UserAdmin(BaseUserAdmin):
    inlines = (AdministratorInline, ContentCreatorInline)


# Re-register UserAdmin
admin.site.unregister(User)
admin.site.register(User, UserAdmin)
admin.site.register(Noticia, NoticiaAdmin)
admin.site.register(Video, VideoAdmin)