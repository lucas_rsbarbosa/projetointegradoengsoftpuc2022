import datetime

from django.contrib.auth.models import User
from django.core.validators import FileExtensionValidator
from django.db import models

# Create your models here.
from django.urls import reverse


class Administrator(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    is_admin = models.BooleanField(null=False, default=False)


class ContentCreator(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    is_content_creator = models.BooleanField(null=False, default=False)


class Noticia(models.Model):
    TIPOS_NOTICIA = (
        (1, 'Publicitaria'),
        (2, 'Boletim'),
    )

    STATUS_CHOICES = [
        ('P', 'Pendente'),
        ('A', 'Aprovada'),
    ]

    titulo = models.CharField(max_length=150, blank=False, null=False)
    resumo = models.TextField(blank=False, null=False)
    #palavrasChave
    slug = models.SlugField(max_length=50, null=True, unique=True)
    thumbnail = models.ImageField(null=True, upload_to="image")
    is_destaque_home = models.BooleanField(verbose_name='Destaque na home')
    tipo_noticia = models.IntegerField(choices=TIPOS_NOTICIA)
    status = models.CharField(blank=False, null=False, default="P", max_length=1, choices=STATUS_CHOICES)
    criado_por = models.ForeignKey(User, on_delete=models.CASCADE, related_name='noticia_criada_por', null=True)
    aprovado_por = models.ForeignKey(User, on_delete=models.CASCADE, related_name='noticia_aprovada_por', null=True)
    data_criacao = models.DateTimeField(auto_now_add=True, null=True)

    def __str__(self):
        return self.titulo

    def get_absolute_url(self):
        return reverse("portal_noticias:detail", kwargs={"slug": self.slug})


class Video(models.Model):

    STATUS_CHOICES = [
        ('P', 'Pendente'),
        ('A', 'Aprovada'),
    ]

    titulo = models.CharField(max_length=150, blank=False, null=False)
    is_destaque_home = models.BooleanField(verbose_name='Destaque na home')
    video = models.FileField(upload_to='video', null=True,
                             validators=[FileExtensionValidator(allowed_extensions=['MOV', 'avi', 'mp4', 'webm', 'mkv'])])

    status = models.CharField(blank=False, null=False, default="P", max_length=1, choices=STATUS_CHOICES)
    criado_por = models.ForeignKey(User, on_delete=models.CASCADE, related_name='video_criado_por', null=True)
    aprovado_por = models.ForeignKey(User, on_delete=models.CASCADE, related_name='video_aprovado_por', null=True)
    data_criacao = models.DateTimeField(auto_now_add=True, null=True)

    def __str__(self):
        return self.titulo
