from django.urls import path

from . import views
from .views import news_view

app_name = "noticias"

urlpatterns = [
    path('', news_view, name='news'),
    path("<slug:slug>/", views.NoticiaDetailView.as_view(), name="detail")
]