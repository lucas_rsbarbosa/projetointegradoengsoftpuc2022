from django.http import HttpResponse
from django.shortcuts import render
from django.views.generic import DetailView

from .models import Noticia, Video


# Create your views here.


class NoticiaDetailView(DetailView):
    model = Noticia


def home_view(request, *args, **kwargs):
    return HttpResponse("<h1> Placeholder para home </h1>")


def index_view(request, *args, **kwargs):
    noticias = Noticia.objects.filter(tipo_noticia=1, status='A', is_destaque_home=True).order_by('-data_criacao')[:10]
    videos = Video.objects.filter(status='A', is_destaque_home=True).order_by('-data_criacao')[:4]
    context = {'noticias': noticias, 'videos': videos}
    return render(request, 'index.html', context)


def search_view(request, *args, **kwargs):
    noticias = Noticia.objects.filter(tipo_noticia=1, status='A', is_destaque_home=True, resumo__icontains=request.GET.get('query','')).order_by('data_criacao')
    context = {'noticias': noticias}
    return render(request, 'portal_noticias/noticia_search.html', context)

def news_view(request, *args, **kwargs):
    return render(request, 'portal_noticias/noticia_detail.html')

